#ifndef ABSTRACTALGORITHM_H
#define ABSTRACTALGORITHM_H

#include <QSharedPointer>
#include <chrono>

class AbstractGame;
class AbstractMove;

using MovePtr = QSharedPointer<AbstractMove>;
using GamePtr = QSharedPointer<AbstractGame>;

/**
 * @brief AbstractAlgorithm
 * Parent class of each algoritm
 */
class AbstractAlgorithm : public QObject
{
    Q_OBJECT
public:
    AbstractAlgorithm() : _timeAllowed(1) {} // one second 

    virtual ~AbstractAlgorithm()
    {
    }

    virtual void start() = 0;

    GamePtr game() const
    {
        return _game;
    }

    void setGame(GamePtr game)
    {
        _game = game;
    }

    double timeAllowed() const
    {
        return _timeAllowed;
    }

    void setTimeAllowed(const double newTimeAllowed)
    {
        if (newTimeAllowed > 0)
            _timeAllowed = newTimeAllowed;
    }

    /**
     * @brief check if the time allowed for the algorithm is elapsed since ::now();
     * 
     * @return true time is up .
     * false time is not up
     */
    bool isElapsed()
    {
        std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - _startTime);
        return (elapsed.count() >= _timeAllowed);
    }

protected:
    GamePtr                                         _game;          // game on which the algorithm will process
    std::chrono::high_resolution_clock::time_point  _startTime;     // start time of the algorithm
    double                                          _timeAllowed;   // max time allowed for the algorithm 

signals:

    /**
     * @brief signal emitted when the processing is completed
     * 
     * @param move best move found by the algorithm
     */
    void resultReady(MovePtr move);                         
};

#endif // ABSTRACTALGORITHM_H
