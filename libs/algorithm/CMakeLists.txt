project (algorithm)

set(SOURCES
    MctsAlgorithm.cpp
    MinMaxAlgorithm.cpp
    RandomAlgorithm.cpp
)
set(INCLUDES
    MctsAlgorithm.h
    MinMaxAlgorithm.h
    RandomAlgorithm.h
    AbstractAlgorithm.h
)

add_library(${PROJECT_NAME} ${SOURCES} ${INCLUDES})

target_include_directories(${PROJECT_NAME} PUBLIC ../game)
target_include_directories(${PROJECT_NAME} PUBLIC ../player)
target_include_directories(${PROJECT_NAME} PUBLIC ../common)

find_package(Qt5Core REQUIRED)

target_link_libraries(${PROJECT_NAME} PUBLIC Qt5::Core)
