#include "MctsAlgorithm.h"

#include <QRandomGenerator>
#include <QDebug>
#include <QThread>
#include <climits>
#include <QtMath>

#include "ModelFactory.h"
#include "AbstractGame.h"
#include "AbstractMove.h"


MctsNode::MctsNode() : _visit(0), _win(0), _parent(nullptr), _game(nullptr)
{
}

MctsNode::MctsNode(int player) : _visit(0), _win(0), _player(player), _parent(nullptr), _game(nullptr)
{
}

MctsNode::~MctsNode()
{
}

double MctsNode::getUct()
{
    if (_parent == nullptr)
        return 0;
    if (_visit == 0)
        return std::numeric_limits<double>::max();
    return ((_win / (double)_visit) + MctsNode::EXPLORATION_PARAM * qPow(qLn((double)_parent->_visit) / (double)_visit, 0.5));
}

void MctsNode::expand()
{
    if (_game->finish() || _children.size() > 0)
        return;
    // create child node for each possible moves
    for (auto move : _game->getMoves())
    {
        MctsNodePtr child = ModelFactory::create<MctsNode>(3 - _player);
        child->_parent = this;
        child->_game = _game->clone();
        move->playInGame(child->_game);
        child->_move = move;
        _children.push_back(child);
    }
}

MctsNodePtr MctsNode::getBestUctChild()
{
    double bestUct = INT_MIN;
    MctsNodePtr bestNode = nullptr;
    for (MctsNodePtr child : _children)
    {
        double tmpUct = child->getUct();
        if (tmpUct > bestUct)
        {
            bestUct = tmpUct;
            bestNode = child;
        }
    }
    return bestNode;
}

MctsNodePtr MctsAlgorithm::selectPromisingNode(MctsNodePtr node)
{
    MctsNodePtr tmpNode = node;
    while (tmpNode->_children.size() != 0)
    {
        tmpNode = tmpNode->getBestUctChild();
    }
    return tmpNode;
}

void MctsAlgorithm::start()
{
    // check game ok
    if (_game == nullptr)
    {
        qDebug() << "_game not initialized";
        return;
    }

    // case: no need to do a MCST
    QVector<MovePtr> moves = _game->getMoves();
    if (moves.size() == 0)
    {
        qDebug() << "Error move.size == 0";
        return;
    }
    else if (moves.size() == 1)
    {
        emit resultReady(moves[0]);
        return;
    }

    // root creation
    _root = ModelFactory::create<MctsNode>(3 - _game->playerTurn());
    _root->_game = _game;
    _rollout = 0;

    //beginning
    _startTime = std::chrono::high_resolution_clock::now();
    while (!isElapsed())
    {
        _rollout++;
        MctsNodePtr node = selectPromisingNode(_root);
        node->expand();
        node = selectPromisingNode(node);
        int winner = simulateRandomPlayout(node->_game->clone());
        node->backPropagation(winner);
    }
    if (_root->_children.size() == 0)
    {
        qDebug() << "problem root no child  ";
        return;
    }
    MctsNodePtr bestNode = _root->_children.at(0);
    for (MctsNodePtr &child : _root->_children)
    {
        if (child->_visit > bestNode->_visit)
            bestNode = child;
    }
    qDebug() << "rollout: " << _rollout;
    qDebug() << "visit: " << bestNode->_visit;
    qDebug() << "win: " << bestNode->_win;
    qDebug() << "ratio: " << bestNode->_win / bestNode->_visit;
    emit resultReady(bestNode->_move);
}

void MctsNode::backPropagation(int winner)
{
    _visit++;
    if (winner == _player)
    {
        _win += MctsNode::WIN_SCORE;
    }
    else if (winner == 0)
    {
        _win += MctsNode::DRAW_SCORE;
    }
    if (_parent)
        _parent->backPropagation(winner);
}

int MctsAlgorithm::simulateRandomPlayout(GamePtr gameDeepCpy)
{
    while (!gameDeepCpy->finish())
    {
        QVector<MovePtr> moves = gameDeepCpy->getMoves();
        moves[rand() % moves.size()]->playInGame(gameDeepCpy);
    }
    return gameDeepCpy->getWinner();
}

MctsAlgorithm::MctsAlgorithm(double timeAllowed) : AbstractAlgorithm(), _root(nullptr), _rollout(0)
{
    setTimeAllowed(timeAllowed);
}

MctsAlgorithm::~MctsAlgorithm()
{

}

