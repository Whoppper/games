#ifndef MCTSALGORITHM_H
#define MCTSALGORITHM_H

#include <QVector>

#include "AbstractAlgorithm.h"

class MctsNode;

using MctsNodePtr = QSharedPointer<MctsNode>;

/**
 * @brief Monte carlo tree search node
 */
class MctsNode
{
public:
    MctsNode();
    MctsNode(int player);
    ~MctsNode();

    /**
     * @brief Get the Best Uct child node
     * 
     * @return node
     */
    MctsNodePtr getBestUctChild();

    /**
     * @brief send the winner of the game to the parent nodes
     * 
     * @param winner winner of the game (0: draw, 1: player1, 2:player2)
     */
    void backPropagation(int winner);

    /**
     * @brief create the children 
     * 
     */
    void expand();

    /**
        @brief  Get the Uct value to choose the promising child  
      
       UCT:  (Node wins / visit) + ExplorationParam * () square( ln(parent visits) / visit)) 
               exploitation                                 exploration
        @return utc 
     */
    double getUct();


    

public:
    int                                 _visit;     // number of times the node has been visited
    double                              _win;       // number of wins (0.5 per draw)
    int                                 _player;    // use to increment the node wins according to the winner of the game
    MctsNode*                           _parent;    // parent Node (dont delete)
    GamePtr                             _game;      // Actual game
    QVector<MctsNodePtr>                _children;  // list of the children
    MovePtr                             _move;      // move that the parent played to create this node

    static constexpr int EXPLORATION_PARAM = 1.41;  // balance between exploration and explotation to choose the promising Node to explore
    static constexpr double WIN_SCORE = 1;          // value to add to _win if the winner owns the node
    static constexpr double DRAW_SCORE = 0.5;       // value to add to _win if there is no winner
};

/**
 * @brief class uses to do a monte carlo tree search to find the next move to play
 * 
 */
class MctsAlgorithm : public AbstractAlgorithm
{
    Q_OBJECT
public:
    MctsAlgorithm(double timeAllowed);
    virtual ~MctsAlgorithm();

    /**
     * @brief start the MCTS
     * 
     */

    virtual void start() override;
    
    /**
     * @brief play random moves for both players until the end of the game
     * 
     * @param copy of the game on which the random moves will be done 
     * @return int 
     */
    int simulateRandomPlayout(GamePtr gameDeepCpy);

    /**
     * @brief Choose the promising node of the tree which will be explore
     * 
     * @param node 
     * @return MctsNodePtr 
     */
    MctsNodePtr selectPromisingNode(MctsNodePtr node);

private:
    MctsNodePtr                 _root;          // Mcts root
    int                         _rollout;       // total simulations 
};

#endif // MCTSALGORITHM_H
