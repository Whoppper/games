#include "MinMaxAlgorithm.h"

#include <QRandomGenerator>
#include <QDebug>

#include "AbstractGame.h"
#include "AbstractMove.h"

void MinMaxAlgorithm::start()
{
    if (!_game)
    {
        qDebug() << "_game not initialized";
        return;
    }

    // case: no need to do a Minmax
    QVector<MovePtr> moves = _game->getMoves();
    _playerNumber = _game->playerTurn();
    if (moves.size() == 0)
    {
        qDebug() << "Error move.size == 0";
        return;
    }
    else if (moves.size() == 1)
    {
        emit resultReady(moves[0]);
        return;
    }

    _startTime = std::chrono::high_resolution_clock::now();
    int totalVisitedState = 0;
    int depth = 1;
    bool gameIsFinished = false;
    while (!isElapsed() && !gameIsFinished)
    {
        /*
            iterative deepening

        */
        _visitedState = 0;
        int eval;
        GamePtr gameCopy = _game->clone();
        try
        {
            QVector<MovePtr> principaleVariationTmp(depth);
            eval = alphabeta(gameCopy, INT_MIN, INT_MAX, depth, 0, principaleVariationTmp, 1);
            // TODO
            if (eval > 2000000000 || eval < -2000000000)
                gameIsFinished = true;
            _principaleVariation = principaleVariationTmp;
            qDebug() << "MinMax depth " << depth << " Eval: " << eval << " Visited States: " << _visitedState;
            totalVisitedState += _visitedState;
        }
        catch (std::exception &e)
        {
            totalVisitedState += _visitedState;
            qDebug() << "Eval: " << eval << " Visited States: " << totalVisitedState;
            QString pv;
            for (MovePtr &move : _principaleVariation)
            {
                if (move != nullptr)
                    pv += move->toString();
            }
            qDebug() << "PrincipaleVariation : " + pv;
            break;
        }
        depth++;
    }

    emit resultReady(_principaleVariation[0]);
}

int MinMaxAlgorithm::alphabeta(GamePtr &game, int alpha, int beta, int depth, int actualDepth, QVector<MovePtr> &principaleVariation, int nodeType)
{
    _visitedState++;
    int best = 0;
    if (isElapsed())
        throw std::invalid_argument("end of time");

    // leaf case
    if (actualDepth == depth || game->finish())
    {
        if (game->finish())
        {
            if (game->getWinner() == _playerNumber)
                return INT_MAX - actualDepth;
            else if (game->getWinner() == 0)
                return 0;
            else
                return INT_MIN + actualDepth;
        }
        else
        {
            return game->eval(_playerNumber);
        }
    }
    // Node Max
    else if (nodeType == 1)
    {
        best = INT_MIN;
        QVector<MovePtr> principaleVariationTmp(depth);
        QVector<MovePtr> moves = game->getMoves();
        // TODO sort
        int i = 0;
        while (i < moves.size())
        {
            GamePtr _gameTmp = game->clone();
            moves[i]->playInGame(_gameTmp);
            int ret = alphabeta(_gameTmp, alpha, beta, depth, actualDepth + 1, principaleVariationTmp, 0);
            //game->undo();
            if (ret > best)
            {
                best = ret;
                principaleVariation = principaleVariationTmp;
                principaleVariation[actualDepth] = moves[i];
            }
            if (best >= beta)
            {
                return best;
            }
            alpha = qMax(alpha, best);
            i++;
        }
    }
    // Node Min
    else if (nodeType == 0)
    {
        best = INT_MAX;
        QVector<MovePtr> principaleVariationTmp(depth);
        QVector<MovePtr> moves = game->getMoves();
        // TODO sort
        int i = 0;
        while (i < moves.size())
        {
            GamePtr _gameTmp = game->clone();
            moves[i]->playInGame(_gameTmp);
            int ret = alphabeta(_gameTmp, alpha, beta, depth, actualDepth + 1, principaleVariationTmp, 1);
            //game->undo();
            if (ret < best)
            {
                best = ret;
                principaleVariation = principaleVariationTmp;
                principaleVariation[actualDepth] = moves[i];
            }
            if (best <= alpha)
            {
                return best;
            }
            beta = qMin(beta, best);
            i++;
        }
    }
    return best;
}

MinMaxAlgorithm::MinMaxAlgorithm(double timeAllowed) : AbstractAlgorithm()
{
    setTimeAllowed(timeAllowed);
}

MinMaxAlgorithm::~MinMaxAlgorithm()
{
}
