#ifndef MINMAXALGORITHM_H
#define MINMAXALGORITHM_H

#include "AbstractAlgorithm.h"
#include <QVector>


class MinMaxAlgorithm : public AbstractAlgorithm
{
    Q_OBJECT
public:
    MinMaxAlgorithm(double timeAllowed);
    virtual ~MinMaxAlgorithm();
    virtual void start() override;

    int alphabeta(GamePtr &game, int alpha, int beta, int depth, int actualDepth, QVector<MovePtr> &principaleVariationTmp, int nodeType);

private:
    QVector<MovePtr>    _principaleVariation;   // list of the best moves according to minmax
    int                 _playerNumber;          // number of visited states
    int                 _visitedState;          // number of visited states
};

#endif // MINMAXALGORITHM_H
