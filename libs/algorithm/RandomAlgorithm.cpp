#include "RandomAlgorithm.h"

#include "AbstractGame.h"
#include "AbstractMove.h"
#include <QRandomGenerator>
#include <QDebug>

void RandomAlgorithm::start()
{
    QVector<MovePtr> moves = _game->getMoves();
    if (moves.size() == 0)
    {
        qDebug() << "Error move.size == 0";
    }
    // int randMove = QRandomGenerator::global()->generate() % moves.size();
    int randMove = rand() % moves.size();
    emit resultReady(moves[randMove]);
}

RandomAlgorithm::RandomAlgorithm(double timeAllowed) : AbstractAlgorithm()
{
    setTimeAllowed(timeAllowed);
}

RandomAlgorithm::~RandomAlgorithm()
{
}
