#ifndef RANDOMALGORITHM_H
#define RANDOMALGORITHM_H

#include "AbstractAlgorithm.h"
#include <QSharedPointer>


class RandomAlgorithm : public AbstractAlgorithm
{
    Q_OBJECT
public:
    RandomAlgorithm(double timeAllowed);
    virtual ~RandomAlgorithm();
    virtual void start() override;
};

#endif // RANDOMALGORITHM_H
