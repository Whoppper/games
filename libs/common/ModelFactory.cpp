#include "ModelFactory.h"

#include "AbstractAlgorithm.h"
#include "RandomAlgorithm.h"
#include "MinMaxAlgorithm.h"
#include "MctsAlgorithm.h"
#include "AbstractPlayer.h"
#include "IA.h"
#include "Human.h"

#include "AbstractGame.h"
#include "Fiar.h"
#include "Uttt.h"

AlgorithmPtr ModelFactory::createAlgoFromString(const QString &name)
{
    AlgorithmPtr algo;
    if (name == "Random")
    {
        algo = ModelFactory::create<RandomAlgorithm>(1.0);
    }
    else if (name == "MinMax")
    {
        algo = ModelFactory::create<MinMaxAlgorithm>(1.0);
    }
    else if (name == "Mcts")
    {
        algo = ModelFactory::create<MctsAlgorithm>(1.0);
    }
    return (algo);
}

GamePtr ModelFactory::createGameFromString(const QString &name)
{
    GamePtr game;
    if (name == "Fiar")
    {
        game = ModelFactory::create<Fiar>();
    }
    else if (name == "Uttt")
    {
        game = ModelFactory::create<Uttt>();
    }
    return (game);
}
