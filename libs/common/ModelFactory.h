#ifndef MODELFACTORY_H
#define MODELFACTORY_H

#include <QSharedPointer>

class AbstractAlgorithm;
class AbstractPlayer;
class AbstractGame;

using PlayerPtr = QSharedPointer<AbstractPlayer> ;
using AlgorithmPtr = QSharedPointer<AbstractAlgorithm>;
using GamePtr = QSharedPointer<AbstractGame>;

class ModelFactory
{
public:
    template <class Type, class... Args>
    static QSharedPointer<Type> create(Args... args)
    {
        QSharedPointer<Type> ptr(new Type(args...));
        return ptr;
    }

    static AlgorithmPtr createAlgoFromString(const QString &name);
    static GamePtr createGameFromString(const QString &name);
};

#endif // MODELFACTORY_H
