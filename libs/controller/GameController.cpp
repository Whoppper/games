#include "GameController.h"

#include "AbstractPlayer.h"
#include "AbstractGame.h"
#include "AbstractMove.h"

#include <QDebug>

GameController::GameController(QObject *parent) : QObject(parent), _gameInProgress(false), _playerTurn(0)
{
}

void GameController::addPlayer(PlayerPtr player)
{
    if (!_gameInProgress)
    {
        _players.push_back(player);
        player->setPlayerNum(_players.size());
    }
}

void GameController::clear()
{
    _gameInProgress = false;
    _players.clear();
    if (_game != Q_NULLPTR)
        _game.reset();
    _playerTurn = 0;
}

void GameController::setGame(GamePtr game)
{
    if (!_gameInProgress)
    {
        _game = game;
    }
}

void GameController::startGame()
{
    qDebug() << "GameController::startGame()" << _players.size();
    if (_players.size() > 0)
    {
        _gameInProgress = true;
        // TODO better
        _players[0]->setGame(_game->clone());
        _players[0]->think();
    }
}

void GameController::onHumanAction(HumanAction action)
{
    if (!_gameInProgress)
    {
        return;
    }
    _players[_playerTurn]->parseUserInput(action);
}

void GameController::moveReceived(MovePtr move)
{
    if (!_gameInProgress || move == Q_NULLPTR)
    {
        return;
    }
    AbstractPlayer *player = qobject_cast<AbstractPlayer *>(sender());
    if (player != Q_NULLPTR)
    {
        if (_players[_playerTurn] == player && move->isValidMove(_game))
        {
            // play the move
            move->playInGame(_game);

            // inform ui or other that the game has changed
            emit gameChanged();
            if (_game->finish())
            {
                _gameInProgress = false;
                qDebug() << "winner: " << _game->getWinner();
                return;
            }
            // the next player must move
            _playerTurn = _playerTurn + 1 >= _players.size() ? 0 : _playerTurn + 1;
            if (_playerTurn < _players.size())
            {
                _players[_playerTurn]->setGame(_game->clone());
                qDebug() << "player: " << _playerTurn << " is thinking";
                _players[_playerTurn]->think();
            }
            else
                qWarning() << "GameController::moveReceived error player turn;";
        }
    }
}
