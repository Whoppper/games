#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include "HumanAction.h"
#include <QObject>
#include <QVector>
#include <QSharedPointer>

class AbstractPlayer;
class AbstractGame;
class AbstractMove;

using PlayerPtr = QSharedPointer<AbstractPlayer> ;
using MovePtr = QSharedPointer<AbstractMove>;
using GamePtr = QSharedPointer<AbstractGame>;

class GameController : public QObject
{
    Q_OBJECT
public:
    explicit GameController(QObject *parent = nullptr);
    void setGame(GamePtr game);
    void startGame();
    void clear();

signals:
    void gameChanged();

public slots:
    void addPlayer(PlayerPtr player);

    /**
     * @brief move received from an Ia or a player
     * 
     * @param move 
     */
    void moveReceived(MovePtr move);
    void onHumanAction(HumanAction action);

private:
    QVector<PlayerPtr>  _players;           // player list
    GamePtr             _game;              // actual game    
    bool                _gameInProgress;    // game is started 
    int                 _playerTurn;        // player turn mkay  
};

#endif // GAMECONTROLLER_H
