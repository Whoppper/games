#include "AbstractGame.h"

const QVector<GameName> AbstractGame::gameList = {GameName::Fiar, GameName::Uttt};

QString AbstractGame::GameNameToString(GameName name)
{
    if (name == GameName::Fiar)
    {
        return "Fiar";
    }
    else if (name == GameName::Uttt)
    {
        return "Uttt";
    }
    return "None";
}
