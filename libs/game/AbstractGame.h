#ifndef GAME_H
#define GAME_H

#include <QVector>
#include <QPair>
#include <QSharedPointer>
#include "HumanAction.h"

constexpr int PLAYER_1 = 1;
constexpr int PLAYER_2 = 2;
constexpr int DRAW = 0;

class FiarMove;
class UtttMove;
class AbstractMove;
class AbstractGame;
class GameUI;

using MovePtr = QSharedPointer<AbstractMove>;
using GamePtr = QSharedPointer<AbstractGame>;

enum class GameName
{
    None,
    Fiar,
    Uttt
};

class AbstractGame
{
public:
    virtual QVector<MovePtr> getMoves() = 0;
    virtual int eval(int player) = 0;
    virtual void undo() = 0;
    virtual int getWinner() = 0;
    virtual bool finish() = 0;
    virtual void display(GameUI &ui) = 0;
    virtual int getMaxPlayersAllowed() = 0;
    virtual int getMinPlayersAllowed() = 0;
    virtual int getMinimumHeight() = 0;
    virtual int getMinimumWidth() = 0;
    virtual QStringList playerAllowed()
    {
        return {};
    };
    virtual MovePtr extractMove(QVector<HumanAction> &actions) = 0;
    virtual GamePtr clone() = 0;

    virtual void play(FiarMove &move)
    {
        Q_UNUSED(move)
    };
    virtual bool isLegalMove(FiarMove &move)
    {
        Q_UNUSED(move);
        return false;
    };

    virtual void play(UtttMove &move)
    {
        Q_UNUSED(move)
    };
    virtual bool isLegalMove(UtttMove &move)
    {
        Q_UNUSED(move);
        return false;
    };

    int playerTurn() const
    {
        return _playerTurn;
    }
    void setPlayerTurn(int newPlayerTurn)
    {
        _playerTurn = newPlayerTurn;
    }

    static const QVector<GameName> gameList;
    static QString GameNameToString(GameName name);

protected:
    int _playerTurn;
};

#endif // GAME_H
