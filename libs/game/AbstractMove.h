#ifndef AbstractMove_H
#define AbstractMove_H

#include "AbstractGame.h"

class AbstractMove
{
public:
    virtual void playInGame(GamePtr game) { Q_UNUSED(game) };
    virtual bool isValidMove(GamePtr game)
    {
        Q_UNUSED(game);
        return false;
    };
    virtual QString toString()
    {
        return "";
    };
    virtual ~AbstractMove(){};
};

#endif // AbstractMove_H
