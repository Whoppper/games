#include "FiarMove.h"

#include "Fiar.h"
#include "AbstractGame.h"

FiarMove::FiarMove()
{
}

FiarMove::FiarMove(int col) : _col(col)
{
}
FiarMove::~FiarMove()
{
}

bool FiarMove::isValidMove(GamePtr game)
{
    return game->isLegalMove(*this);
}

void FiarMove::playInGame(GamePtr game)
{
    game->play(*this);
}

int FiarMove::col() const
{
    return _col;
}

void FiarMove::setCol(int col)
{
    _col = col;
}
