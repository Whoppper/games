#ifndef FIARMOVE_H
#define FIARMOVE_H

#include "AbstractMove.h"

class AbstractGame;

class FiarMove : public AbstractMove
{
public:
    FiarMove();
    FiarMove(int col);
    virtual ~FiarMove();
    virtual void playInGame(GamePtr game) override;
    virtual bool isValidMove(GamePtr game) override;
    int col() const;
    void setCol(int col);
    virtual QString toString() override
    {
        return QString("col: %1  ").arg(QString::number(_col));
    }

private:
    int _col;
};

#endif // FIARMOVE_H
