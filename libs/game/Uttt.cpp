#include "Uttt.h"

#include "ModelFactory.h"
#include "AbstractMove.h"
#include "UtttMove.h"
#include "GameUI.h"
#include <QDebug>
#include <QPoint>

//TODO CLEAN

const int Ttt::sEvalBoardMax = 8;

Ttt::Ttt() : _board(9, -1), _winner(-1), _countMoves(0)
{
}

Ttt::~Ttt()
{
}

void Ttt::play(int pos, int player)
{
    if (_winner != -1 || !isLegal(pos))
    {
        qDebug() << "Ttt::play --- Illegal move";
        return;
    }
    _board[pos] = player;
    _countMoves++;
    updateWin(player);
}

void Ttt::updateWin(int player)
{
    if ((_board[0] == player && _board[1] == player && _board[2] == player) ||
        (_board[3] == player && _board[4] == player && _board[5] == player) ||
        (_board[6] == player && _board[7] == player && _board[8] == player) ||
        (_board[0] == player && _board[3] == player && _board[6] == player) ||
        (_board[1] == player && _board[4] == player && _board[7] == player) ||
        (_board[2] == player && _board[5] == player && _board[8] == player) ||
        (_board[0] == player && _board[4] == player && _board[8] == player) ||
        (_board[2] == player && _board[4] == player && _board[6] == player))
        _winner = player;
    else if (_countMoves == 9)
        _winner = 0;
}

int Ttt::getWinner()
{
    return _winner;
}

int Ttt::getWinnerIfDraw()
{
    int p1 = 0;
    int p2 = 0;
    int i = 0;
    while (i < 9)
    {
        if (_board[i] == 1)
        {
            p1++;
        }
        if (_board[i] == 2)
        {
            p2++;
        }
        i++;
    }
    if (p1 > p2)
        return 1;
    else if (p1 == p2)
        return 0;
    else
        return 2;
}

bool Ttt::isLegal(int pos)
{
    if (_winner != -1 || pos < 0 || pos > 9)
        return false;
    return _board[pos] == -1;
}

QVector<int> Ttt::getMoves()
{
    QVector<int> moves;
    if (_winner != -1)
        return moves;
    for (int i = 0; i < _board.size(); i++)
    {
        if (_board[i] == -1)
            moves.push_back(i);
    }
    return moves;
}

int Ttt::eval(int player)
{
    int score = 0;
    if (_winner == 1)
        score = sEvalBoardMax;
    if (_winner == 2)
        score = -sEvalBoardMax;
    if (_winner != -1)
    {
        if (player == 2)
            return -score;
        return score;
    }
    int lineP1[8] = {0};
    int lineP2[8] = {0};

    int i = 0;
    while (i < 3)
    {
        if (_board[i] == 1)
            lineP1[0]++;
        if (_board[i] == 2)
            lineP2[0]++;
        //
        if (_board[i + 3] == 1)
            lineP1[1]++;
        if (_board[i + 3] == 2)
            lineP2[1]++;
        //
        if (_board[i + 6] == 1)
            lineP1[2]++;
        if (_board[i + 6] == 2)
            lineP2[2]++;
        //
        if (_board[3 * i] == 1)
            lineP1[3]++;
        if (_board[3 * i] == 2)
            lineP2[3]++;
        //
        if (_board[3 * i + 1] == 1)
            lineP1[4]++;
        if (_board[3 * i + 1] == 2)
            lineP2[4]++;
        //
        if (_board[3 * i + 2] == 1)
            lineP1[5]++;
        if (_board[3 * i + 2] == 2)
            lineP2[5]++;
        //
        if (_board[3 * i + i] == 1)
            lineP1[6]++;
        if (_board[3 * i + i] == 2)
            lineP2[6]++;
        //
        if (_board[2 * i + 2] == 1)
            lineP1[7]++;
        if (_board[2 * i + 2] == 2)
            lineP2[7]++;

        i++;
    }

    i = 0;
    while (i < 8)
    {
        if (lineP1[i] == 2 && lineP2[i] == 0)
            score += 3;
        else if (lineP1[i] == 1 && lineP2[i] == 0)
            score += 1;
        else if (lineP1[i] == 0 && lineP2[i] == 1)
            score -= 1;
        else if (lineP1[i] == 0 && lineP2[i] == 2)
            score -= 3;
        i++;
    }

    if (score >= sEvalBoardMax - 1)
        score = sEvalBoardMax - 2;
    if (score <= -sEvalBoardMax + 1)
        score = -sEvalBoardMax + 2;

    if (player == 2)
        return -score;
    return score;
}

namespace
{
    void getCoordFromPosition(QPoint position, QPair<int, int> &coord)
    {
        coord.first = position.y() / 60;
        coord.second = position.x() / 60;
    }

    int getUtttIndex(int row, int col)
    {
        int a = (row / 3) * 3;
        int b = (col / 3);

        int utttIndex = a + b;
        return utttIndex;
    }

    int getTttIndex(int row, int col)
    {
        int a = (row % 3) * 3;
        int b = (col % 3);

        int tttIndex = a + b;
        return tttIndex;
    }

    int getRowfromIndex(int utttIndex, int tttIndex)
    {
        int a = (utttIndex / 3) * 3;
        int b = (tttIndex / 3);

        int row = a + b;
        return row;
    }

    int getColfromIndex(int utttIndex, int tttIndex)
    {
        int a = (utttIndex % 3) * 3;
        int b = (tttIndex % 3);

        int col = a + b;
        return col;
    }
}

Uttt::Uttt() : _boards(10),
               _winner(-1),
               _nextboardIndex(-1)
{
    _playerTurn = PLAYER_1;
}

Uttt::~Uttt()
{
}

int Uttt::getMinimumWidth()
{
    return 9 * 60;
}
int Uttt::getMinimumHeight()
{
    return 9 * 60;
}

void Uttt::display(GameUI &ui)
{
    ui.displayGame(*this);
}

bool Uttt::finish()
{
    return _winner != -1;
}

QStringList Uttt::playerAllowed()
{
    return (QStringList{"Human", "Mcts", "MinMax", "Random"});
}

MovePtr Uttt::extractMove(QVector<HumanAction> &actions)
{
    for (int i = 0; i < actions.size(); i++)
    {
        if (actions[i].action == ActionType::MouseClick)
        {
            QSharedPointer<UtttMove> move = ModelFactory::create<UtttMove>();
            QPair<int, int> coord;
            getCoordFromPosition(actions[i].position, coord);
            move->setCol(coord.second);
            move->setRow(coord.first);
            actions.clear();
            return move;
        }
    }
    actions.clear();
    return Q_NULLPTR;
}

bool Uttt::isLegalMove(UtttMove &move)
{
    int col = move.col();
    int row = move.row();
    return isLegal(row, col);
}

bool Uttt::isLegal(int row, int col)
{
    if (finish() || col > 8 || row > 8 || col < 0 || row < 0)
        return false;
    int utttIndex = getUtttIndex(row, col);
    int tttIndex = getTttIndex(row, col);
    if (utttIndex == _nextboardIndex || (_nextboardIndex == -1 && _boards[utttIndex].getWinner() == -1))
    {
        if (_boards[utttIndex].getWinner() != -1 || _boards[utttIndex][tttIndex] != -1)
        {
            return false;
        }
        return true;
    }
    else
    {
        return false;
    }
    return true;
}

QVector<MovePtr> Uttt::getMoves()
{
    QVector<MovePtr> moves;
    if (_winner != -1)
        return moves;
    if (_nextboardIndex == -1)
    {
        for (int utttIndex = 0; utttIndex < 9; utttIndex++)
        {
            for (int tttIndex : _boards[utttIndex].getMoves())
            {
                QSharedPointer<UtttMove> move = ModelFactory::create<UtttMove>();
                move->setRow(getRowfromIndex(utttIndex, tttIndex));
                move->setCol(getColfromIndex(utttIndex, tttIndex));
                moves.push_back(move);
            }
        }
    }
    else
    {
        for (int tttIndex : _boards[_nextboardIndex].getMoves())
        {
            QSharedPointer<UtttMove> move = ModelFactory::create<UtttMove>();
            move->setRow(getRowfromIndex(_nextboardIndex, tttIndex));
            move->setCol(getColfromIndex(_nextboardIndex, tttIndex));
            moves.push_back(move);
        }
    }
    return moves;
}

int Uttt::getMaxPlayersAllowed()
{
    return 2;
}
int Uttt::getMinPlayersAllowed()
{
    return 2;
}

int Uttt::eval(int player)
{
    int i = 0;

    int lineP1[8] = {0};
    int lineP2[8] = {0};
    int lineScore[8] = {0};
    int tttEvals[9];
    while (i < 9)
    {
        tttEvals[i] = _boards[i].eval(player);
        i++;
    }

    i = 0;
    while (i < 3)
    {
        if (_boards[i].getWinner() == 1)
            lineP1[0]++;
        if (_boards[i].getWinner() == 2)
            lineP2[0]++;
        lineScore[0] += tttEvals[i];
        //
        if (_boards[i + 3].getWinner() == 1)
            lineP1[1]++;
        if (_boards[i + 3].getWinner() == 2)
            lineP2[1]++;
        lineScore[1] += tttEvals[i + 3];
        //
        if (_boards[i + 6].getWinner() == 1)
            lineP1[2]++;
        if (_boards[i + 6].getWinner() == 2)
            lineP2[2]++;
        lineScore[2] += tttEvals[i + 6];
        //
        if (_boards[3 * i].getWinner() == 1)
            lineP1[3]++;
        if (_boards[3 * i].getWinner() == 2)
            lineP2[3]++;
        lineScore[3] += tttEvals[3 * i];
        //
        if (_boards[3 * i + 1].getWinner() == 1)
            lineP1[4]++;
        if (_boards[3 * i + 1].getWinner() == 2)
            lineP2[4]++;
        lineScore[4] += tttEvals[3 * i + 1];
        //
        if (_boards[3 * i + 2].getWinner() == 1)
            lineP1[5]++;
        if (_boards[3 * i + 2].getWinner() == 2)
            lineP2[5]++;
        lineScore[5] += tttEvals[3 * i + 2];
        //
        if (_boards[3 * i + i].getWinner() == 1)
            lineP1[6]++;
        if (_boards[3 * i + i].getWinner() == 2)
            lineP2[6]++;
        lineScore[6] += tttEvals[3 * i + i];
        //
        if (_boards[2 * i + 2].getWinner() == 1)
            lineP1[7]++;
        if (_boards[2 * i + 2].getWinner() == 2)
            lineP2[7]++;
        lineScore[7] += tttEvals[2 * i + 2];

        i++;
    }

    i = 0;
    int evaluation = 0;
    while (i < 8)
    {
        if (lineP1[i] == 2 && lineP2[i] == 0 && lineScore[i] > 2 * Ttt::sEvalBoardMax + 2)
            evaluation += lineScore[i] * 7;
        else if (lineP1[i] == 2 && lineP2[i] == 0 && lineScore[i] < -(2 * Ttt::sEvalBoardMax + 2))
            evaluation += lineScore[i] * 7;
        else if (lineP1[i] == 2 && lineP2[i] == 0)
            evaluation += lineScore[i] * 4;
        else if (lineP1[i] == 1 && lineP2[i] == 0)
            evaluation += lineScore[i] * 2;
        else if (lineP1[i] == 0 && lineP2[i] == 1)
            evaluation += lineScore[i] * 2;
        else if (lineP1[i] == 0 && lineP2[i] == 2)
            evaluation += lineScore[i] * 4;
        else if (lineP1[i] == 0 && lineP2[i] == 0)
            evaluation += lineScore[i];

        i++;
    }
    return evaluation;
}

void Uttt::play(UtttMove &move)
{
    if (_winner != -1)
        return;
    int col = move.col();
    int row = move.row();

    int utttIndex = getUtttIndex(row, col);
    int tttIndex = getTttIndex(row, col);

    if (!isLegalMove(move))
    {
        qWarning() << "invalid move " << row << " " << col;

        return;
    }
    _boards[utttIndex].play(tttIndex, _playerTurn);
    _nextboardIndex = tttIndex;
    if (_boards[_nextboardIndex].getWinner() != -1)
    {
        _nextboardIndex = -1;
    }
    int result = _boards[utttIndex].getWinner();
    if (result != -1)
    {
        _boards[9].play(utttIndex, result);
        if (_boards[9].getWinner() != -1)
        {
            _winner = _boards[9].getWinner();
            if (_winner == 0)
                _winner = _boards[9].getWinnerIfDraw();
        }
    }
    _playerTurn = 3 - _playerTurn;
}

void Uttt::undo()
{
    qDebug() << "Uttt::undo() TODO";
}

int Uttt::getWinner()
{
    return _winner;
}

GamePtr Uttt::clone()
{
    Uttt *uttt = new Uttt();
    uttt->_playerTurn = _playerTurn;
    uttt->_boards = _boards;
    uttt->_winner = _winner;
    uttt->_nextboardIndex = _nextboardIndex;

    GamePtr cpy(uttt);
    return cpy;
}
