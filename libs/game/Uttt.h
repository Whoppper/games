#ifndef UTTT_H
#define UTTT_H

#include "AbstractGame.h"

class GameUI;
class AbstractMove;

class Ttt
{
public:
    Ttt();
    ~Ttt();

    void play(int pos, int player);
    int getWinner();
    QVector<int> getMoves();
    void updateWin(int player);
    int getWinnerIfDraw();
    bool isLegal(int pos);
    int eval(int player);

    int operator [](int i) const {return _board[i];}

    static const int sEvalBoardMax;


private:
    QVector<int> _board;
    int _winner;
    int _countMoves;

};

class Uttt : public AbstractGame
{
public:
    Uttt();
    virtual ~Uttt();

    virtual QVector<MovePtr> getMoves() override;
    virtual int eval(int player) override;
    virtual void undo() override;
    virtual bool finish() override;
    virtual int getWinner() override;
    virtual int getMinimumWidth() override;
    virtual int getMinimumHeight() override;
    virtual int getMaxPlayersAllowed() override;
    virtual int getMinPlayersAllowed() override;
    virtual void display(GameUI &ui) override;
    virtual QStringList playerAllowed() override;
    virtual GamePtr clone() override;
    virtual MovePtr extractMove(QVector<HumanAction> &actions) override;
    virtual void play(UtttMove &move) override;
    virtual bool isLegalMove(UtttMove &move) override;
    bool isLegal(int row, int col);
    QVector<Ttt> boards() {return _boards;}
private:
    QVector<Ttt> _boards;
    int _winner;
    int _nextboardIndex;

private:
};

#endif // FOURINAROW_H
