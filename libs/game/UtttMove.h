#ifndef UTTTMOVE_H
#define UTTTMOVE_H

#include "AbstractMove.h"

class AbstractGame;

class UtttMove : public AbstractMove
{
public:
    UtttMove();
    UtttMove(int row, int col);
    virtual ~UtttMove();
    virtual void playInGame(GamePtr game) override;
    virtual bool isValidMove(GamePtr game) override;
    int col() const;
    int row() const;
    void setCol(int col);
    void setRow(int row);
    virtual QString toString() override
    {
        return QString("row: %1 , col: %2  ").arg(QString::number(_row)).arg(QString::number(_col));
    }

private:
    int _row;
    int _col;
};

#endif // FIARMOVE_H
