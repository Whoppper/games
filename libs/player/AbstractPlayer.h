#ifndef AbstractPlayer_H
#define AbstractPlayer_H

#include "HumanAction.h"
#include <QSharedPointer>
#include <QObject>

class AbstractGame;
class AbstractMove;
class AbstractAlgorithm;
class GameController;
class GameUI;

using MovePtr = QSharedPointer<AbstractMove>;
using GamePtr = QSharedPointer<AbstractGame>;
using AlgorithmPtr = QSharedPointer<AbstractAlgorithm>;
using GameControllerPtr = QSharedPointer<GameController>;
using GameUIPtr = QSharedPointer<GameUI>;

class AbstractPlayer : public QObject
{
    Q_OBJECT
public:
    AbstractPlayer(){};
    AbstractPlayer(const GamePtr game) : _game(game){};

    /**
     * @brief Start the algorithm or wait for the player's move
     * 
     */
    virtual void think() = 0;

    virtual void setConnection(GameUIPtr ui, GameControllerPtr controller) = 0;

    void setGame(GamePtr game)
    {
        _game = game;
    }

    void setPlayerNum(int num)
    {
        _playerNum = num;
    }

    int PlayerNum()
    {
        return _playerNum;
    }
    /**
     * @brief transform the player click-move to a game move
     * 
     * @param action 
     */
    virtual void parseUserInput(HumanAction action) { Q_UNUSED(action) };

signals:
/**
 * @brief send the chosen move to the controller
 * 
 * @param move 
 */
    void sendMove(MovePtr move);

protected:
    GamePtr _game;      // actual game
    int     _playerNum; // player num
};

#endif // AbstractPlayer_H
