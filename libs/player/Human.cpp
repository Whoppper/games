#include "Human.h"

#include "AbstractGame.h"
#include "AbstractMove.h"
#include "GameController.h"
#include "GameUI.h"
#include <QDebug>

Human::Human(GamePtr game) : AbstractPlayer(game), _needToPlay(false)
{
}

Human::Human() : _needToPlay(false)
{
}

void Human::think()
{
    _needToPlay = true;
}

void Human::parseUserInput(HumanAction action)
{
    if (!_needToPlay) // if is not our turn
        return;
    _actions.push_back(action);
    MovePtr newMove = _game->extractMove(_actions); // _game can remove some action in _actions
    if (newMove != Q_NULLPTR && newMove->isValidMove(_game))
    {
        _needToPlay = false;
        _actions.clear();
        emit sendMove(newMove);
    }
}

void Human::setConnection(QSharedPointer<GameUI> ui, QSharedPointer<GameController> controller)
{
    Q_UNUSED(ui);
    connect(this, &Human::sendMove, controller.get(), &GameController::moveReceived);
}
