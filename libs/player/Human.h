#ifndef HUMAN_H
#define HUMAN_H


#include <QVector>
#include <QSharedPointer>
#include <QPoint>

#include "HumanAction.h"
#include "AbstractPlayer.h"

class AbstractGame;
class AbstractMove;

class Human : public AbstractPlayer
{
    Q_OBJECT
public:
    Human();
    Human(GamePtr game);
    virtual void think() override;
    virtual void setConnection(QSharedPointer<GameUI> ui, QSharedPointer<GameController> controller) override;
    virtual void parseUserInput(HumanAction action) override;

private:
    bool _needToPlay;
    QVector<HumanAction> _actions; // QVector si l'human doit faire plusieurs clicks pour jouer un move
};

#endif // HUMAN_H
