#include "IA.h"

#include "AbstractAlgorithm.h"
#include "AbstractGame.h"
#include "AbstractMove.h"
#include "GameController.h"

#include "FiarMove.h"
#include <QObject>
#include <QDebug>

#include <math.h>


double IA::squareRoot(const double a) {
    double b = sqrt(a);
    if(b != b) {
        return -1.0;
    }else{
        return sqrt(a);
    }
}

IA::IA()
{
}

IA::IA(GamePtr game, AlgorithmPtr algorithm) : AbstractPlayer(game), _algorithm(algorithm)
{
    // move the algo execution in another thread to let the IHM unabled
    _thread = new QThread();
    _algorithm->moveToThread(_thread);
    connect(_algorithm.get(), &AbstractAlgorithm::resultReady, this, &IA::onResultReady);
    connect(_thread, &QThread::started, _algorithm.get(), &AbstractAlgorithm::start);
}

void IA::think()
{
    qDebug() << "IA is thinking...";
    _algorithm->setGame(_game);
    // the current algo will calculate the next move
    _thread->start();
}

void IA::setAlgorithm(const AlgorithmPtr &algorithm)
{
    _algorithm = algorithm;
    connect(_algorithm.get(), &AbstractAlgorithm::resultReady, this, &IA::onResultReady);
}

void IA::setConnection(QSharedPointer<GameUI> ui, QSharedPointer<GameController> controller)
{
    Q_UNUSED(ui);
    connect(this, &IA::sendMove, controller.get(), &GameController::moveReceived);
}

void IA::onResultReady(MovePtr move)
{
    //qDebug() << "IA send move..." << move;
    _thread->quit();
    emit sendMove(move);
}

IA::~IA()
{
    if (_thread)
    {
        _thread->quit();
        // TODO create and call algo.stop(). remplacer les fonctions des algos avec des slots pour pouvoir les arreter avec QAbstractEventDispatcher 
        if (!_thread->wait(3000))
        {
            qDebug() << "call thread terminate";
            _thread->terminate();
            _thread->wait();
        }
        delete _thread;
        _thread = nullptr;
    }
}
