#ifndef IA_H
#define IA_H

#include "AbstractPlayer.h"
#include "AbstractAlgorithm.h"
#include "AbstractMove.h"
#include <QSharedPointer>
#include <QThread>
#include <QMetaType>



class IA : public AbstractPlayer
{
    Q_OBJECT
public:
    virtual ~IA();
    IA();
    IA(GamePtr game, AlgorithmPtr algorithm);
    virtual void think() override;
    void setAlgorithm(const AlgorithmPtr &algorithm);
    virtual void setConnection(QSharedPointer<GameUI> ui, QSharedPointer<GameController> controller) override;

    // to try unit test, to remove
    static double squareRoot(const double a);
signals:
    void operate();

private slots:
    void onResultReady(MovePtr move);

private:
    QThread *_thread;
    AlgorithmPtr _algorithm;
};

// Q_DECLARE_METATYPE(MovePtr);

#endif // IA_H
