#ifndef GAMEUI_H
#define GAMEUI_H

#include <QWidget>
#include <QSharedDataPointer>
#include <QPainter>
#include "HumanAction.h"

class Fiar;
class Uttt;
class AbstractGame;

using GamePtr = QSharedPointer<AbstractGame>;


class GameUI : public QWidget
{
    Q_OBJECT
public:
    GameUI(QWidget *parent = nullptr, GamePtr game = nullptr);
    void displayGame(Fiar &fiar);
    void displayGame(Uttt &uttt);
    GamePtr game() const;
    void setGame(const GamePtr &game);

protected:
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;

public slots:
    void needToRefresh();

signals:
/**
 * @brief send user inputs
 * 
 * 
 * @param action 
 */
    void newHumanAction(HumanAction action);

private:
    GamePtr _game;
};

#endif // GAMEUI_H
