#include "NewGameDialog.h"
#include <QDebug>

#include "AbstractMove.h"
#include "AbstractPlayer.h"
#include "RandomAlgorithm.h"
#include "AbstractAlgorithm.h"
#include "IA.h"
#include "Human.h"
#include "GameUI.h"
#include "GameController.h"
#include "ModelFactory.h"
#include "Fiar.h"

namespace
{
    void clearLayout(QBoxLayout *layout)
    {
        QLayoutItem *child;
        while ((child = layout->takeAt(0)) != 0)
        {
            delete child;
        }
    }
}

NewGameDialog::NewGameDialog(QDialog *parent) : QDialog(parent)
{
    _vlayout = new QVBoxLayout(this);
    _playerWidget = new QWidget(this);
    _hlayout = new QHBoxLayout();
    _gameCombo = new QComboBox(this);
    for (const GameName &name : AbstractGame::gameList)
    {
        _gameCombo->addItem(AbstractGame::GameNameToString(name));
    }
    _valider = new QPushButton("OK", this);
    layout()->addWidget(_gameCombo);
    _playerWidget->setLayout(_hlayout);
    layout()->addWidget(_playerWidget);
    layout()->addWidget(_valider);
    connect(_valider, &QPushButton::clicked, this, &NewGameDialog::createGame);
    connect(_gameCombo, &QComboBox::currentTextChanged, this, &NewGameDialog::gameSelected);
    resize(600, 400);
}

void NewGameDialog::resetDialog()
{
    _playerCombo.clear();
    _game.clear();
    _players.clear();
    clearLayout(_hlayout);
    gameSelected(AbstractGame::GameNameToString(GameName::Fiar));
}

void NewGameDialog::gameSelected(const QString &gameName)
{
    GamePtr tmpgame = ModelFactory::createGameFromString(gameName);
    if (tmpgame == nullptr)
        return;
    clearLayout(_hlayout);
    _playerCombo.clear();
    int maxPlayers = tmpgame->getMaxPlayersAllowed();
    for (int i = 0; i < maxPlayers; i++)
    {
        QComboBox *combo = new QComboBox(this);
        combo->addItems(tmpgame->playerAllowed());
        _hlayout->addWidget(combo);
        _playerCombo.push_back(combo);
    }
}

QVector<QSharedPointer<AbstractPlayer>> NewGameDialog::players() const
{
    return _players;
}

GamePtr NewGameDialog::game() const
{
    return _game;
}

void NewGameDialog::createGame()
{
    _players.clear();
    _game = ModelFactory::createGameFromString(_gameCombo->currentText());
    int nbPlayers = 0;
    for (QComboBox *combo : _playerCombo)
    {
        if (combo->currentText() != "None")
            nbPlayers++;
    }
    if (nbPlayers < _game->getMinPlayersAllowed())
    {
        qDebug() << "Minimun Players: " << _game->getMinPlayersAllowed() << " " << nbPlayers;
        QDialog::reject();
        return;
    }

    int i = 0;
    while (i < _playerCombo.size())
    {
        if (_playerCombo[i]->currentText() != "Human" && _playerCombo[i]->currentText() != "None")
        {
            AlgorithmPtr algo = ModelFactory::createAlgoFromString(_playerCombo[i]->currentText());
            IA *ia = new IA(_game->clone(), algo);
            PlayerPtr p(ia);
            _players.push_back(p);
        }
        else if (_playerCombo[i]->currentText() == "Human")
        {
            Human *human = new Human(_game->clone());
            PlayerPtr p(human);
            _players.push_back(p);
        }
        i++;
    }
    QDialog::accept();
}
