#ifndef NEWGAMEDIALOG_H
#define NEWGAMEDIALOG_H

#include <QWidget>
#include <QDialog>
#include <QVector>
#include <QLayout>
#include <QSharedPointer>
#include <QComboBox>
#include <QPushButton>

#include "AbstractGame.h"
#include "mainwindow.h"

class AbstractPlayer;
class AbstractGame;

using PlayerPtr = QSharedPointer<AbstractPlayer> ;
using GamePtr = QSharedPointer<AbstractGame>;

class NewGameDialog : public QDialog
{
    Q_OBJECT
public:
    explicit NewGameDialog(QDialog *parent = nullptr);
    QVector<PlayerPtr> players() const;
    GamePtr game() const;
    void resetDialog();

private:
    QVector<PlayerPtr> _players;
    GamePtr _game;

    QComboBox *_gameCombo;
    QVector<QComboBox *> _playerCombo;

    QVBoxLayout *_vlayout;
    QHBoxLayout *_hlayout;
    QPushButton *_valider;
    QWidget *_playerWidget;

private slots:
    void createGame();
    void gameSelected(const QString &gameName);
};

#endif // NEWGAMEDIALOG_H
