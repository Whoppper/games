#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QMenuBar>
#include <QMenu>
#include <QSharedPointer>
#include <QScopedPointer>
#include <QLayout>

class NewGameDialog;
class GameUI;
class GameController;
class AbstractGame;

using GamePtr = QSharedPointer<AbstractGame>;
using NewGameDialogPtr = QSharedPointer<NewGameDialog>;
using GameControllerPtr = QSharedPointer<GameController>;
using GameUIPtr = QSharedPointer<GameUI>;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    GamePtr game() const;
    void setGame(const GamePtr &game);

private slots:
    void newGame();

private:
    void setMenus();

    GamePtr             _game;
    GameUIPtr           _ui;
    GameControllerPtr   _controller;
    NewGameDialogPtr    _gameDialog;

    QMenu       *_menuFile;
    QAction     *_newGame;
    QAction     *_startGame;
    QHBoxLayout *_hlayout;
};
#endif // MAINWINDOW_H
