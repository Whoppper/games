
#include "gtest/gtest.h"

#include "IA.h"

TEST(SquareRootTest, PositiveNos) { 
    ASSERT_EQ(6, IA::squareRoot(36.0));
    ASSERT_EQ(18.0, IA::squareRoot(324.0));
    ASSERT_EQ(25.4, IA::squareRoot(645.16));
    
    ASSERT_EQ(0, IA::squareRoot(0.0));
}
 
TEST(SquareRootTest, NegativeNos) {
    ASSERT_EQ(-1.0, IA::squareRoot(-15.0));
    ASSERT_EQ(-1.0, IA::squareRoot(-0.2));
}

TEST(SquareRootTest, Error) {
    ASSERT_NE(25, IA::squareRoot(625));
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}